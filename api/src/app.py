from flask import Flask
from flask_caching import Cache

from .es import init_es

app = Flask(__name__)
cache = Cache(app,config={'CACHE_TYPE': 'redis'})

@app.route('/')
def index():
    return 'hello'

from .utils import add_global_error_handler
add_global_error_handler(app)

from .routes import v1
app.register_blueprint(v1)

init_es()
