from . import utils

indices = {
    'request': {
        "ip": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        },
        "location": {
            "type": "geo_point"
        },
        "msg": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        },
        "timestamp": {
            "type": "date"
        },
        "type": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        }
    },
    'covid-csse': {
        "Admin2": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        },
        "Combined_Key": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        },
        "FIPS": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        },
        "Province_State": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        },
        "Country_Region": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        },
        "Confirmed": {
            "type": "integer",
        },
        "Deaths": {
            "type": "integer",
        },
        "Recovered": {
            "type": "integer",
        },
        "injesttime": {
            "type": "date"
        },
        "location": {
            "type": "geo_point"
        },
        "source": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                }
            }
        },
        "timestamp": {
            "type": "date"
        }
    }
}

def init_es():
    for index, mapping in indices.items():
        if not utils.es.indices.exists(index=index):
            utils.es.indices.create(
                index=index,
                body={
                    "mappings": {
                        "properties": mapping,
                    }
                }
            )
