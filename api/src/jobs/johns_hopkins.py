from datetime import datetime
from multiprocessing import Pool
from io import StringIO
import dateutil.parser
import tempfile
import requests
import shutil
import json
import glob
import csv
import os

from .. import utils


def injest():
    savedir = os.getcwd()
    tempdir = tempfile.mkdtemp()
    os.chdir(tempdir)
    os.system('wget https://github.com/CSSEGISandData/COVID-19/archive/master.zip && unzip master.zip')
    os.chdir('COVID-19-master/csse_covid_19_data/csse_covid_19_daily_reports')
    reports = list(sorted(glob.glob('*.csv')))
    pool = Pool(processes=32)

    pool.map(injest_csv, reports)

    os.chdir(savedir)
    shutil.rmtree(tempdir)


def injest_csv(filename):
    reader=csv.DictReader(open(filename))
    for row in reader:
        row=dict(row)

        try:
            row['location'] = {
                'lat': float(row['Lat']),
                'lon': float(row['Long_' if 'Long_' in row else 'Long'])
            }

            del row['Lat']
            del row['Long_' if 'Long_' in row else 'Long']
        except:
            pass

        try:
            utils.esindex(
                index='covid-csse',
                source='csse',
                **row,
                timestamp=dateutil.parser.parse(filename.rstrip('.csv')),
                injesttime=datetime.utcnow()
            )
        except:
            pass
    print('processed:', filename, flush=True)
