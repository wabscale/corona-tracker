from flask import request, redirect, url_for, flash, render_template, Blueprint, Response

from ..app import cache
from ..utils import enqueue, log_event, esindex, json
from ..jobs import injest as injest_jh

v1 = Blueprint('v1', __name__, url_prefix='/v1')


@v1.route('/injestjh')
def index():
    enqueue(injest_jh)
    return 'enqueued job\n'
