from elasticsearch import Elasticsearch
from flask import request, Response
from dataclasses import dataclass
from werkzeug import exceptions
from datetime import datetime
from functools import wraps
from geoip import geolite2
from redis import Redis
from json import dumps
from os import environ
from rq import Queue
import traceback

es = Elasticsearch(['http://elasticsearch:9200'])

"""
We will use rq to enqueue and dequeue jobs.
"""
def enqueue(func, *args):
    """
    Enqueues a job on the redis cache
    :func callable: any callable object
    :args tuple: ordered arguments for function
    """
    with Redis(host='redis') as conn:
        q = Queue(connection=conn, default_timeout=3600)
        q.enqueue(func, *args)

def get_request_ip():
    """
    Since we are using a request forwarder,
    the real client IP will be added as a header.
    This function will search the expected headers
    to find that ip.
    """
    def check(header):
        """get header from request else empty string"""
        return request.headers[
            header
        ] if header in request.headers else ''

    def check_(headers, n=0):
        """check headers based on ordered piority"""
        if n == len(headers):
            return ''
        return check(headers[n]) or check_(headers, n+1)

    return str(check_([
        'x-forwarded-for', # highest priority
        'X-Forwarded-For',
        'x-real-ip',
        'X-Real-Ip',       # lowest priority
    ]) or request.remote_addr or 'N/A')


def log_event(log_type, message_func):
    """
    Use this to decorate a route and add logging.
    The message_func should be a calleble object
    that returns a string to be logged.

    eg.

    @log_event('LOG-TYPE', lambda: 'somefunction was just called')
    def somefunction(arg1, arg2):
        ....

    :log_type str: log type to noted in event
    :message_func callable: function to return message to be logged
    """
    def decorator(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            ip = get_request_ip()
            location = geolite2.lookup(ip)
            es.index(index='request', body={
                'type': log_type.lower(),
                'msg': message_func(),
                'location': location.location[::-1] if location is not None else location,
                'ip': ip,
                'timestamp': datetime.utcnow(),
            })

            return function(*args, **kwargs)
        return wrapper
    return decorator


def esindex(index='error', **kwargs):
    """
    Index anything with elasticsearch

    :kwargs dict:
    """
    es.index(index=index, body={
        'timestamp': datetime.utcnow(),
        **kwargs,
    })


def jsonify(data):
    """
    Wrap a data response to set proper headers for json
    """
    res = Response(dumps(data))
    res.headers['Content-Type'] = 'application/json'
    res.headers['Access-Control-Allow-Origin'] = 'https://nyu.cool' if not environ.get('DEBUG', False) else 'https://localhost'
    return res

def json(func):
    """
    Wrap a route so that it always converts data
    response to proper json.

    @app.route('/')
    @json
    def test():
        return {
            'success': True
        }
    """
    @wraps(func)
    def json_wrap(*args, **kwargs):
        data = func(*args, **kwargs)
        return jsonify(data)
    return json_wrap


def add_global_error_handler(app):
    @app.errorhandler(Exception)
    def global_err_handler(error):
        tb = traceback.format_exc() # get traceback string
        esindex(
            'error',
            type='global-handler',
            logs=request.url + ' - ' + get_request_ip() + '\n' + tb,
            submission=None,
            netid=None,
        )
        if isinstance(error, exceptions.NotFound):
            return '', 404
        return 'err'
