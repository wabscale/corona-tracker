#!/bin/bash


#
# Usage:
#   ./backup.sh # backs up elasticsearch and db data
#

set -ex

cd $(dirname $(realpath $0))
cd ..

TIMESTAMP="$(date +%s)"
BASE=".backups/${TIMESTAMP}"

ES_CONTAINER="$(docker ps | nice grep elasticsearch | head -1 | awk '{print $(NF)}')"

mkdir -p ${BASE}

docker run \
       --rm \
       --volumes-from ${ES_CONTAINER} \
       -v $(pwd):/backup \
       alpine \
       tar czf /backup/${BASE}/es.tar.gz \
       /usr/share/elasticsearch/data
