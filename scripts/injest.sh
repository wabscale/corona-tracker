#!/bin/sh

set -e

curl 'https://api.localhost/v1/injest?type=confirmed&url=https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv' -ik

curl 'https://api.localhost/v1/injest?type=deaths&url=https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv' -ik
